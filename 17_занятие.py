import sqlite3

conn = sqlite3.connect('persons.db')
cur = conn.cursor()
cur.execute("CREATE TABLE if not exists workers(id integer PRIMARY KEY, name text, lname text, dept text, age integer, "
            "experience integer)")

# cur.execute("""INSERT INTO workers(id, name, lname, dept, age
#                 , experience) VALUES('1', 'Иван', 'Петров', 'экспертиза', '52', '10');""")
# user = ('2', 'Сергей', 'Сергеев', 'хозотдел', '45', '6')
# cur.execute("INSERT INTO workers VALUES(?, ?, ?, ?, ?, ?);", user)
# users = [('3', 'Федор', 'Федосеев', 'смета', '30', '2'), ('4', 'Василина', 'Афонина', 'безопасность', '26', '1')]
# cur.executemany("INSERT INTO workers VALUES(?, ?, ?, ?, ?, ?);", users)

# cur.execute("UPDATE workers SET dept = 'проектирование' WHERE age > 45")

# cur.execute("SELECT * FROM workers")
# cur.execute("SELECT lname, dept, experience FROM workers")
# checklist = cur.fetchall()
# for worker in checklist:
#     print(worker)
#
# cur.execute("DROP table if exists workers")

conn.commit()
conn.close()
